import Modal from "../../components/Modal"
import { useSelector, useDispatch } from 'react-redux';
import { toggleLogin, toggleRegister } from "./UserSlice";

const Register = () => {
const show=useSelector(state=>state.user.register)
const dispatch=useDispatch()

const handleReg=()=>{
  dispatch(toggleRegister(false))
 dispatch(toggleLogin(true))
}


  return (<>
    {show  && <Modal classm="mt-5 text-4xl" type="Регистрация" width="w-[384px] md:w-[629px] h-[838px]">
    <div className="flex gap-4 mt-8">
    <h1 className=''>Иметь аккаунт?</h1>
    <h1 className='text-sky-500 hover:text-fuchsia-500 hover:underline ' onClick={handleReg}>Войти</h1>
    </div>
<div className="flex mt-8">
<label >
<h2 >Ваше имя</h2>
<div className="border mt-2 w-[337px] md:w-[420px] py-3 md:py-4 rounded-xl bg-white"> <input type="text" className=' px-4 outline-none w-28 md:w-96' placeholder='Ваше имя...' /></div>
</label>
</div>
<div className="flex mt-8">
<label >
<h2 >Электронная почта или номер телефона</h2>
<div className="border mt-2 w-[337px] md:w-[420px] py-3 md:py-4 rounded-xl bg-white"> <input type="text" className=' px-4 outline-none w-28 md:w-96' placeholder='Email...' /></div>
</label>
</div>
<div className="flex mt-8">
<label >
<h2 >Пароль</h2>
<div className="border mt-2 w-[337px] md:w-[420px] py-3 md:py-4 rounded-xl bg-white"> <input type={`password`} className=' px-4 outline-none w-28 md:w-96' placeholder='Password...' /></div>
</label>
</div>
<div className="flex mt-8">
<label >
<h2 >Подтвердить пароль</h2>
<div className="border mt-2 w-[337px] md:w-[420px] py-3 md:py-4 rounded-xl bg-white"> <input type="text" className=' px-4 outline-none w-28 md:w-96' placeholder='Password...' /></div>
</label>
</div>
<div className="flex gap-4 mt-8 items-center">
<input type="checkbox" className='cursor-pointer w-4 h-4 accent-inherit bg-green-500   focus:accent-lime-600 scale-150   '/><h3>Я согласен с Условиями и Политикой конфиденциальности</h3>
</div>
<button className='mt-8 orange px-8 py-3 text-white  mb-12 rounded-lg'>Регистрация</button>
  </Modal>}
  </>)
}

export default Register