import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  show:false,
  search:"",
  login:false,
  register:false,
  oform:false,
  pocupka:false
};
const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
   
    toggleShow:(state,action)=>{
        state.show=action.payload
    },
     setSearchValue:(state,action)=>{
        state.search=action.payload
     },
     toggleLogin:(state,action)=>{
      state.login=action.payload
  },
     toggleRegister:(state,action)=>{
      state.register=action.payload
  },
     toggleOform:(state,action)=>{
      state.oform=action.payload
  },
     togglePocupka:(state,action)=>{
      state.pocupka=action.payload
  },
    
  },
});
export const {toggleShow,setSearchValue,toggleLogin,toggleRegister,toggleOform,togglePocupka} = userSlice.actions;
export default userSlice.reducer;