import { useRef } from "react";
import Modal from "../../components/Modal"
import { useSelector, useDispatch } from 'react-redux';

const Login = () => {
  const dispatch=useDispatch()
  const show=useSelector(state=>state.user.login)
  const email=useRef()
  const password=useRef()
  const handleSubmit=()=>{
const obj={
email: email.current.value,
password: password.current.value
}
console.log(obj);
    dispatch()
  }
  return (
    <div>
       {show &&  <Modal classm={"text-4xl md:mt-5"} type={"Войти"} width="w-[384px] md:w-[629px] md:h-[554px] h-[432px]">
       <form onSubmit={handleSubmit}>
        <div className="flex mt-6">
  <label >
    <h2 className="text-sm">Электронная почта или номер телефона</h2>
   <div className="border mt-2 w-[337px] md:w-[420px] py-3 md:py-4 rounded-xl bg-white"> <input type="text" className='px-2 md:px-4 outline-none w-full md:w-96' placeholder='Email...' required ref={email}/></div>
  </label>
</div>
        <div className="flex mt-6">
  <label >
    <h2 >Пароль</h2>
   <div className="border mt-2  w-[337px] md:w-[420px] py-3 md:py-4 rounded-xl bg-white"> <input type="password" className='px-2 md:px-4 outline-none w-full md:w-96' placeholder='Password...' required ref={password}/></div>
  </label>
</div>
<div className="mt-6 text-sky-500 hover:text-fuchsia-500 hover:underline">Забыли свой пароль ?</div>
<button type="submit" className=' orange px-8 py-3 text-white mt-6 mb-12 rounded-lg'>Войти</button>
</form>  </Modal>}
    </div>
  )
}

export default Login