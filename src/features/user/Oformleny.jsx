import { useRef, useState } from "react";
import Modal from "../../components/Modal"
import { useSelector } from 'react-redux';
const Oformleny = () => {
 const  count=useRef()
 const  name=useRef()
 const  country=useRef()
 const  phone=useRef()
 const  oblst=useRef()
 const  punkt=useRef()
 const  adress=useRef()

 const [check,setCheck]=useState(false)

 const handleSubmit = (e) => {
e.preventDefault()
const obj={
  count:count.current.value,
  name:name.current.value,
  country:country.current.value,
  phone:phone.current.value,
  oblst:oblst.current.value,
  punkt:punkt.current.value,
  adress:adress.current.value,
  priorty:check
}
console.log(obj);
 }
const show=useSelector((state)=>state.user.oform)
  return (
    <div>
     {show &&  <Modal classm={"md:text-4xl text-2xl  "} type="Оформление заказа" width={"w-[420px] md:w-[1110px] h-[944px] md:h-[648px]  "}>
 <form onSubmit={handleSubmit}>      <h2 className="mt-4">Купить Пеноплекс Основа</h2>
        <div className="flex gap-3 md:gap-16 flex-wrap">
          <div className="">
            <div className="flex mt-4 flex-wrap">
              <label>
                <h2>Введите Штук</h2>
                <div className="border mt-3 w-[320px] md:w-[420px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="number"
                    className=" px-4 outline-none w-full"
                    placeholder="Count..."
                    ref={count}
                    required
                  />
                </div>
              </label>
            </div>
            <div className="flex mt-2 md:mt-4">
              <label>
                <h2>Введите имя</h2>
                <div className="border mt-3 w-[320px] md:w-[420px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="text"
                    className=" px-4 outline-none w-full"
                    placeholder="Name..."
                    ref={name}
                    required
                  />
                </div>
              </label>
            </div>
            <div className="flex mt-2 md:mt-4">
              <label>
                <h2>Введите город / район </h2>
                <div className="border mt-3 w-[320px] md:w-[420px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="text"
                    className=" px-4 outline-none w-full"
                    placeholder="город..."
                    ref={country}
                    required
                  />
                </div>
              </label>
            </div>
          </div>
          <div className="">    
            <div className="flex mt-2 md:mt-4">
              <label>
                <h2>Введите номер телефона </h2>
                <div className="border mt-3 w-[320px] md:w-[420px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="number"
                    className=" px-4 outline-none w-full"
                    placeholder="Phone..."
                    ref={phone}
                    required
                  />
                </div>
              </label>
            </div>
            <div className="flex mt-2 md:mt-4">
              <label>
                <h2>Введите область </h2>
                <div className="border mt-3 w-[320px] md:w-[420px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="text"
                    className=" px-4 outline-none w-full"
                    placeholder="Shtat..."
                    ref={oblst}
                    required
                  />
                </div>
              </label>
            </div>
            <div className="flex mt-2 md:mt-4">
              <label>
                <h2>Введите населённый пункт </h2>
                <div className="border mt-3 w-[320px] md:w-[420px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="text"
                    className=" px-4 outline-none w-full"
                    placeholder="пункт..."
ref={punkt}
                    required
                  />
                </div>
              </label>
            </div>
          </div>
        </div>
       
        <div className="flex mt-2 md:mt-4">
              <label>
                <h2>Введите адресс</h2>
                <div className="border mt-3 w-[320px] md:w-[1020px] py-2 md:py-2.5 rounded-xl bg-white">
                  {" "}
                  <input
                    type="text"
                    className=" px-4 outline-none w-full"
                    placeholder="Aдресс..."
                    ref={adress}
                    required
                  />
                </div>
              </label>
            </div>
            <div className="flex gap-4 mt-4 items-center">
<input    type="checkbox" onChange={(e)=>e.target.value=="on"?setCheck(true):setCheck(false)} className='cursor-pointer w-4 h-4 accent-inherit bg-green-500   focus:accent-lime-600 scale-150   ' /><h3>Я согласен с <span className="text-sky-500 hover:text-fuchsia-500 hover:underline  ">правилами публичной оферты</span></h3>
</div>
<button type="submit" className=' orange px-8 py-2 md:py-2.5 text-white my-6 rounded-lg'  >Оформить заказ</button>
</form> 
      </Modal>}
    </div>
  );
};

export default Oformleny;
