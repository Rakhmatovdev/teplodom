import Modal from "../../components/Modal"
import { useSelector } from 'react-redux';

const Pocupka = () => {
    const show=useSelector(state=>state.user.pocupka)
  return (
    <div>
{show && <Modal type={"Спасиба за покупка !"} width={"w-[752px] h-[423px]"} classm={"text-4xl justify-center pr-12 mt-20"}  >
   <div className="text-center"> <h3 className=" mt-4 text-xl pr-12">Ваш номер заказ №127836</h3>
    <button className="mr-12 px-4 mt-10 text-lg tracking-wider py-3 orange text-white rounded-xl">Главная страница</button></div>
</Modal>}
    </div>
  )
}

export default Pocupka