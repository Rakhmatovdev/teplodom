import Login from './Login';
import Oformleny from './Oformleny';
import Register from './Register';
import { useDispatch } from 'react-redux';
import { toggleRegister } from './UserSlice';
import Pocupka from './Pocupka';
const User = () => {
 const  dispatch=useDispatch()
  return (
    <div className='w-full'>
      <button className='orange px-4 py-2 rounded-xl text-white my-[300px] mx-auto' onClick={()=>dispatch(toggleRegister(true))}>Register</button>
      <Pocupka/>
  <Register/>
  <Login/>
    <Oformleny/>
    </div>
  )
}

export default User
