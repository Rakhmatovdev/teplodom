import { Outlet } from "react-router-dom"
import Footer from "../components/Footer"
import Navbar from "../components/Navbar"
import Hero from "../components/Hero"
import Login from "../features/user/Login"
import Register from "../features/user/Register"
import Oformleny from "../features/user/Oformleny"

const RootLayout = () => {
  return (<div className="sticky ">
  <Login/>
      <Register/>
      <Oformleny/>
    <div className="flex flex-col    bg-stone-100  ">
       
    <Navbar/>
    <Hero/>
    <main className="flex-1">
        <Outlet/>
    </main>
    <Footer/>
    </div>
</div> )
}

export default RootLayout