import { Link, NavLink,useNavigate  } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setSearchValue, toggleRegister } from "../features/user/UserSlice";
import { useState } from "react";
import { useFetch } from '../hooks/useFetch';
const Navbar = () => {
  const dispatch = useDispatch();
  const searchValue = useSelector((state) => state.user.search);
  const value = useSelector((state) => state);
  const navigate = useNavigate();
  const [naw,setNaw]=useState(false)
  const handleSubmit = (e) => {
    e.preventDefault();
    navigate("/filter");
  };

const {data}=useFetch("http://localhost:7777/links")
  return (
    <div className=" container  mt-16 mx-auto">
      <div className="">
        <div className="flex items-center justify-between  ">
          <NavLink to="/" className="flex">
            <img className="bg-cover " src="/photos/logo.png" alt="user" />
            <div className="flex flex-col ml-1">
              <span className="text-2xl font-extrabold text-[#F87C3A] ">
                TEPLODOM
              </span>
              <span className="text-sm ml-1 text-black">
                Интернет магазин <br />
                строй материалов
              </span>
            </div>
          </NavLink>
          <form
            onSubmit={handleSubmit}
            className=" hidden  md:flex flex-row  border-2 h-[56px] rounded-2xl  px-4 pr-40 font-medium items-center bg-white"
          >
            <img
              src="/photos/search.png"
              alt=""
              className="inline-flex h-[26px]"
            />
            <input
              type="text"
              className=" py-2 outline-none px-2 "
              autoComplete="off"
              placeholder="Поиск..."
              value={searchValue}
              onChange={(e) => dispatch(setSearchValue(e.target.value))}
            />
          </form>

          <div className="gap-4 flex items-center relative text-black">
            <NavLink
              to="like"
              className=" fa-solid fa-heart rounded-full bg-slate-50 fa-lg px-4 py-6 focus:text-red-400  "
            >
              {value.like.like.length > 0 && (
                <span className="absolute left-8 top-0  w-7 px-2 py-1 rounded-full bg-red-500 text-white text-xs font-semibold flex justify-center items-center ">
                  {value.like.like.length}
                </span>
              )}
            </NavLink>
            <NavLink
              to="backet"
              className="bx bxs-shopping-bags  fa-lg px-4 py-4 relative rounded-full bg-slate-50 focus:text-red-400"
            >
              {value.backet.backet.length > 0 && (
                <span className="absolute left-8 top-0  w-7 px-2 py-1 rounded-full bg-red-500 text-white text-xs font-semibold flex justify-center items-center ">
                  {value.backet.backet.length}
                </span>
              )}
            </NavLink>
            <NavLink
              to="user"
              className="fa-solid fa-user fa-lg px-4 py-6 rounded-full bg-slate-50 focus:text-red-400"
            >
              <span className="hidden lg:flex ml-6"> Профиль</span>
            </NavLink>
          </div>
        </div>
        <div className="flex md:hidden items-center justify-between mt-3 px-3">
          <div className="" onClick={() => setNaw(prev=>!prev)}>
            <img src="/photos/Bar.png" alt="" />
          </div>
          <form onSubmit={handleSubmit} className=" flex bg-white  flex-row  border-2 h-[44px] rounded-2xl pr-10 font-medium items-center">
            <img
              src="/photos/search.png"
              alt=""
              className="inline-flex h-[26px] px-2"
            />
            <input
              type="text"
              className="pr-20 py-2 outline-none "
              autoComplete="off"
              placeholder="Поиск..."
              value={searchValue}
              onChange={(e)=>dispatch(setSearchValue(e.target.value))}
            />
          </form>
          <div className="">
            <img src="/photos/sidebar.png" alt="" />
          </div>
        </div>
        {naw && (
        <section className={`flex flex-col pt-[30px] absolute z-10 bg-white  w-full h-screen px-2 top-0 left-0 rounded-r-[15px] sidebar`}>
          <div className=" flex items-center justify-between">
            <div className=" flex justify-start items-center gap-1">
              <img src="/photos/logo.png" alt="navbar logo" className="  w-[49px] h-[49px] sm:w-[70px] sm:h-[70px] object-contain" />
              <div className=" flex flex-col items-center">
                <h2 className=" text-[14]  sm:text-[24px] font-bold text-orange-500">TEPLODOM</h2>
                <p className=" flex flex-col text-[9.1px] items-center font-normal sm:text-[13px] leading-[105%]">
                  <span>Интернет магазин</span> <span>строй материалов</span>
                </p>
              </div>
            </div>
            <div className=" h-[44px] w-[44px]   rounded-full flex items-center justify-center  cursor-pointer">
              <i className="fa-solid fa-x fa-lg h-[16px] w-[16px]" onClick={() => setNaw((prev) => !prev)}> </i>
            </div>
          </div>
          <div className="my-10 flex flex-col ">
            {data.map(({ id, title, link }) => (
              <Link key={id} to={link} onClick={()=>setNaw(false)} className={`py-3  text-[18px] font-bold ${id == data.length ? "" : "border-b-2  border-black pr-4"} active:-rotate-1  focus:-rotate-1 transition-all duration-300 `}>
                {title}
              </Link>
            ))}
          </div>
        </section>
      )}

      </div>
    </div>
  );
};

export default Navbar;
