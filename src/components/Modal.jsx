import { toggleLogin, toggleOform, togglePocupka, toggleRegister } from "../features/user/UserSlice";
import { useDispatch } from 'react-redux';

const Modal = ({ children, classm, type,width, }) => {
  const dispatch=useDispatch()
  const handleShow=()=>{
dispatch(toggleLogin(false))
dispatch(toggleRegister(false))
dispatch(toggleOform(false))
dispatch(togglePocupka(false))
  }
  console.log(close);
  return (
    <div className=" absolute  z-10  bottom-0  top-0 pt-20  mb-full bg-black/30  flex justify-center h-full w-full">
      <main
        className={`blur-none ${width}   border pl-8 pr-3 md:pl-16 md:pr-6 py-4 rounded-2xl bg-stone-50 relative `}
      >
        <div className="flex justify-end">
        
          <i onClick={handleShow}>
            <img src="/photos/xxx.svg" alt="img"  className=""/>
          </i>
        </div>
        <div className={`  flex  w-full  ${classm}`}>
          <h1 className=" font-medium tracking-wider">{type}</h1>
        </div>
        {children}
      </main>
    </div>
  );
};

export default Modal;
