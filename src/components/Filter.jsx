import { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getProducts } from "../features/products/ProductSlice";
import { addBacket } from "../features/backet/BacketSlice";
import { addLike } from "../features/like/LikeSlie";
import { message } from "antd";
import { Link } from "react-router-dom";
const Filter = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProducts(204));
  }, []);

  const country=useRef("UZ")
  const firstPrice=useRef()
  const secondPrice=useRef()
  const brand=useRef()
  const vmestimost=useRef("КГ")
  const [color, setColor] = useState("white");

  const handeSubmit =(e)=>{
    e.preventDefault()
  console.log({
    country:country.current.value,
    firstPrice:firstPrice.current.value,
    secondPrice:secondPrice.current.value,
    brand:brand.current.value,
    vmestimost:vmestimost.current.value,
    color,
  });   
  }

 

  const handleBacket = (product) => {
    dispatch(addBacket(product));
    message.open({
      type: "success",
      content: "добавлено в корзину",
    });
  };

  const handleLike = (product) => {
    dispatch(addLike(product));
    message.open({
      type: "success",
      content: "добавлено в избранное",
    });
  };

  const data = useSelector((state) => state.products.products);
  return (
    <div className="bg-stone-100">
      <div className="container mx-auto mt-14">
        <div className="flex flex-wrap gap-4 ">
          <form className="border-2 w-[360px] h-[669px] bg-white rounded-2xl">
            <h1 className="text-2xl font-semibold tracking-wider mt-8 ml-4 ">
              Филтр
            </h1>
            <div className="px-4 mx-auto">
              <label
                form="countries"
                className="block mb-2 mt-6 text-sm font-medium    text-gray-900 "
              >
                Страна-производитель
              </label>
              <select
                id="countries"
                className=" border border-gray-300 bg-stone-100   text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
                value={country}
               ref={country}
              >
                <option value="UZ">Uzbekistan</option>
                <option value="US">United States</option>
                <option value="RU">Russia</option>
                <option value="DE">Germany</option>
              </select>
            </div>
            <div className="px-4 mx-auto">
              <label className="block mb-2 mt-8 text-sm font-medium    text-gray-900 ">
              Цена
              </label>
              <div className=" py-3 rounded-lg outline-none  flex items-center justify-between bg-stone-100">
                <input
                  type="number"
                  placeholder="От"
                  className="outline-none px-2 w-40 bg-transparent"
                 ref={firstPrice}
                />
                <input
                  type="number"
                  placeholder="До"
                  className="outline-none px-2 w-40 bg-transparent   "
                  ref={secondPrice}
                />
              </div>
            </div>
            <div>
            <div className=" mb-[40px] px-4 mx-auto">
              <p className=" font-medium mb-2 mt-8 text-md tracking-wider">Цвет</p>
              <div className="9 flex justify-between items-center">
                <div onClick={()=>setColor("black")} className=" cursor-pointer w-[24px] md:w-[30px] h-[24px] md:h-[30px] rounded-full bg-black ring-offset-2 ring-2 ring-black"></div>
                <div onClick={()=>setColor("white")} className=" cursor-pointer w-[24px] md:w-[30px] h-[24px] md:h-[30px] rounded-full bg-slate-100 ring-offset-2 ring-2 ring-slate-100"></div>
                <div onClick={()=>setColor("red")} className=" cursor-pointer w-[24px] md:w-[30px] h-[24px] md:h-[30px] rounded-full bg-red-500  ring-offset-2 ring-2 ring-red-500"></div>
                <div onClick={()=>setColor("orange")} className=" cursor-pointer w-[24px] md:w-[30px] h-[24px] md:h-[30px] rounded-full bg-orange-500   ring-offset-2 ring-2 ring-orange-500"></div>
                <div onClick={()=>setColor("green")} className=" cursor-pointer w-[24px] md:w-[30px] h-[24px] md:h-[30px] rounded-full bg-green-500 ring-offset-2 ring-2 ring-green-500"></div>
                <div onClick={()=>setColor("purple")} className=" cursor-pointer w-[24px] md:w-[30px] h-[24px] md:h-[30px] rounded-full bg-purple-500 ring-offset-2 ring-2 ring-purple-500"></div>
              </div>
            </div>

              <div className="px-4 mx-auto mt-4">
                <label className="">Бренды товары</label>
                <div className=" mt-2 px-2 py-3 rounded-xl  bg-stone-100">
                  <input
                    type="text"
                    placeholder="Brands..."
                    className="outline-none bg-transparent "
                    ref={brand}
                  />
                </div>
              </div>
              <div className="px-4 mx-auto">
                <label
                  form="countries"
                  className="block mb-2 mt-8 text-sm font-medium    text-gray-900 "
                >
                  Вместимость
                </label>
                <select
                  id="countries"
                  className=" border border-gray-300 bg-stone-100   text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
              
                 ref={vmestimost}
                >
                  <option
                    defaultValue={"Вместимость"}
                    className=" w-full bg-stone-100 my-3"
                  >
                    Вместимость
                  </option>
                  <option value={"Литр"} className=" w-full bg-stone-100 py-3">
                    Литр
                  </option>
                  <option value={"Кг"} className=" w-full bg-stone-100 py-3">
                    Кг
                  </option>
                  <option
                    value={"Диаметр"}
                    className=" w-full bg-stone-100 py-3"
                  >
                    Диаметр
                  </option>
                </select>
              </div>
              <div className="px-4 mx-auto flex mt-6 justify-between gap-2 text-xl">
                <div className="bg-stone-100 flex-1 py-3 justify-center  gap-2 flex items-center rounded-xl">
                  <i className="fa-solid fa-x"></i>
                  <button type="button">Очистить</button>{" "}
                </div>
                <div className="orange text-white flex-1 py-3 justify-center  gap-2 flex items-center rounded-xl">
                  <i className="fa-solid fa-magnifying-glass"></i>

                  <button type="submit" onClick={handeSubmit}>Найти</button>{" "}
                </div>
              </div>
            </div>
          </form>
          <div className="flex-1">
            <div className="flex justify-between items-end    ">
              <div className="text-3xl tracking-wider font-medium  ">
                Товары по поиску ({data.length*10})
              </div>
              <div className="text-sky-500 hover:text-fuchsia-500 cursor-pointer">
                Смотреть все &gt;
              </div>
            </div>
            <div className="flex flex-col mt-8 gap-4 mb-8">
              {data?.map((product) => {
                return (
                  <div
                    className="flex border justify-between px-4 rounded-2xl bg-white"
                    key={product.id}
                  >
                    <div className="flex py-4 gap-8">
                      <Link
                        to={`/products/${product.id}`}
                        className="w-[170px] h-[170px] rounded-xl border py-4"
                      >
                        <img
                          src={product.image}
                          alt=""
                          className="h-full w-full bg-cover"
                        />
                      </Link>
                      <div className="flex flex-col">
                        <div className="text-xl tracking-wider font-semibold w-[350px]">
                          {product.name}
                        </div>
                        <div className="mt-16 pt-2 text-2xl font-semibold">
                          {product.price / 1000} 000 СУМ
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col justify-around">
                      <button
                        onClick={() => handleLike(product)}
                        className="px-4 py-4 border rounded-xl orange text-white"
                      >
                        <i className="fa-regular fa-heart fa-xl"></i>
                      </button>
                      <button
                        onClick={() => handleBacket(product)}
                        className="px-4 py-4 border rounded-xl orange text-white"
                      >
                        <i className="bx bxs-shopping-bags fa-xl"></i>
                      </button>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Filter;
